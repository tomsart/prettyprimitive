Pretty Primitive
================

Simple [Godot](https://godotengine.org) panel addon to assist in creation of
basic boxes, spheres, cylinders, cones, capsules, and planes, together
with colliders and RigidBody, StaticBody, Area, or Spatial parents.

Simply download this repository to addons/prettyprimitive in your
project, and activate in project settings.

Built in Godot 3.2.

![Screenshot](screenshot.png)

