tool
extends Control

var edi # an EditorInterface, cache for convenience

func _ready():
	print ("Primitive panel active!")
	if Engine.editor_hint:
		var plugin = EditorPlugin.new()
		edi = plugin.get_editor_interface() # now you always have the interface
		plugin.queue_free()
		print ("Primitive panel active 2! edi: ", edi)
		
		_on_XYZ_toggled($ScrollContainer/VBoxContainer/Boxes/XYZ.pressed)
		_on_XY_toggled($ScrollContainer/VBoxContainer/Plane/XY.pressed)


#func _enter_tree():
#	$HBoxContainer/MakeCube.connect("pressed", self, "AddCube")

enum Parent {
	 None,
	 Rigid,
	 Static,
	 AreaParent,
	 SpatialParent
	 }

# Return { axis:int, nodes_type:Nodes }
func GetOptions():
	return { "axis":           $ScrollContainer/VBoxContainer/Options/AxisOptions.selected,
			 "parent_type":    $ScrollContainer/VBoxContainer/Options/ParentOptions.selected,
			 "with_mesh" :     $ScrollContainer/VBoxContainer/Options/Mesh.pressed,
			 "with_collider" : $ScrollContainer/VBoxContainer/Options/Collider.pressed,
			 "with_mat" :      $ScrollContainer/VBoxContainer/Material/WithMat.pressed,
			 "with_color" :    $ScrollContainer/VBoxContainer/ColorMat/WithColor.pressed
			}


# Return [ top, mesh, collider ]
# There will always at most be those three new nodes. owner still
# needs to be set on them.
func BuildBase(options, parent, theowner):
	print ("buildbase col: ", options["with_collider"] )
	
	var themesh : MeshInstance = null
	if options["with_mesh"]: themesh = MeshInstance.new()
		
	var col = null
	if options["with_collider"]: col = CollisionShape.new()
	
	var pnt = parent
	match options["parent_type"]:
		Parent.Rigid:
			pnt = RigidBody.new()
		Parent.Static:
			pnt = StaticBody.new()
		Parent.AreaParent:
			pnt = Area.new()
		Parent.SpatialParent:
			pnt = Spatial.new()
			
	if pnt != parent:
		parent.add_child(pnt)
		pnt.owner = theowner
	if themesh:
		pnt.add_child(themesh)
		themesh.owner = theowner
	if col:
		pnt.add_child(col)
		col.owner = theowner
	return [ pnt, themesh, col ]

# Return { owner, parents:[dest1, dest2, ... ] }
func GetDestParents():
	var scene_root = get_tree().edited_scene_root
	var new_owner = scene_root.get_tree().edited_scene_root
	
	var dest
	var sel = edi.get_selection().get_selected_nodes()
	if sel.size() > 0:
		dest = sel
	else:
		dest = [ scene_root ]
		
	return { "owner": new_owner, "parents": dest }


func AdjustMaterial(meshi, options):
	if options["with_color"]: AddColor(meshi)
	#elif options["with_mat"]: AddMaterial(meshi)
	
func AddColor(meshi : MeshInstance):
	var mat = SpatialMaterial.new()
	mat.albedo_color = $ScrollContainer/VBoxContainer/ColorMat/ColorPickerButton.color
	meshi.set_surface_material(0, mat)

func AddMaterial(meshi : MeshInstance):
	print_debug("IMPLEMENT ME!!!")


func _on_MakeCube_pressed():
	print ("Add Cube pressed!")
	AddCube()


# Adds a new cube to any selected objects, or scene root.
func AddCube():
	print ("Add cube primitive")
	if !edi: return null # in case we tried to run scene, instead of use through editor
	
	var sizex = $ScrollContainer/VBoxContainer/Boxes/Size.value
	var sizey = $ScrollContainer/VBoxContainer/Boxes/Y.value
	var sizez = $ScrollContainer/VBoxContainer/Boxes/Z.value
	if !$ScrollContainer/VBoxContainer/Boxes/XYZ.pressed:
		sizey = sizex
		sizez = sizex
	
	var dest = GetDestParents()
	var options = GetOptions()
	
	print ("options: ", options)
	print ("dest:    ", dest)
	
	var cube = null
	if options["with_mesh"]:
		cube = CubeMesh.new()
		cube.size = Vector3(sizex, sizey, sizez)
	var shape = null
	if options["with_collider"]:
		shape = BoxShape.new()
		shape.extents = Vector3(sizex/2, sizey/2, sizez/2)
	
	for p in dest["parents"]:
		var nodes = BuildBase(options, p, dest["owner"])
		if nodes[0] != p: nodes[0].name = "Cube"
		if nodes[1]:
			nodes[1].mesh = cube
			nodes[1].name = "CubeMesh"
			AdjustMaterial(nodes[1], options)
		if nodes[2]: #collider
			nodes[2].shape = shape
			nodes[2].name = "CubeShape"


func _on_MakeSphere_pressed():
	print ("Add sphere primitive")
	if !edi: return null # in case we tried to run scene, instead of use through editor
	
	var size = $ScrollContainer/VBoxContainer/Spheres/Size.value
	
	var dest = GetDestParents()
	var options = GetOptions()
	
	var m = null
	if options["with_mesh"]:
		m = SphereMesh.new()
		m.radius = size
		m.height = 2 * size
	var shape = null
	if options["with_collider"]:
		shape = SphereShape.new()
		shape.radius = size
	
	for p in dest["parents"]:
		var nodes = BuildBase(options, p, dest["owner"])
		if nodes[0] != p: nodes[0].name = "Sphere"
		if nodes[1]:
			nodes[1].mesh = m
			nodes[1].name = "SphereMesh"
			AdjustMaterial(nodes[1], options)
		if nodes[2]: #collider
			nodes[2].shape = shape
			nodes[2].name = "SphereShape"


func _on_MakeCapsule_pressed():
	print ("Add capsule primitive")
	if !edi: return null # in case we tried to run scene, instead of use through editor
	
	var height = $ScrollContainer/VBoxContainer/Capsule/Height.value
	var radius = $ScrollContainer/VBoxContainer/Capsule/Radius.value
	
	var dest = GetDestParents()
	var options = GetOptions()
	
	var m = null
	if options["with_mesh"]:
		m = CapsuleMesh.new()
		m.radius = radius
		m.mid_height = height - 2*radius
	var shape = null
	if options["with_collider"]:
		shape = CapsuleShape.new()
		shape.radius = radius
		shape.height = height - 2*radius
	
	for p in dest["parents"]:
		var nodes = BuildBase(options, p, dest["owner"])
		if nodes[0] != p: nodes[0].name = "Capsule"
		if nodes[1]:
			nodes[1].mesh = m
			nodes[1].name = "CapsuleMesh"
			AdjustMaterial(nodes[1], options)
		if nodes[2]: #collider
			nodes[2].shape = shape
			nodes[2].name = "CapsuleShape"
			
		match options["axis"]:
			0:
				if nodes[1]: nodes[1].rotation_degrees = Vector3(0,90,90)
				if nodes[2]: nodes[2].rotation_degrees = Vector3(0,90,90)
			1:
				if nodes[1]: nodes[1].rotation_degrees = Vector3(90,0,0)
				if nodes[2]: nodes[2].rotation_degrees = Vector3(90,0,0)


func _on_MakeCylinder_pressed():
	print ("Add cylinder primitive")
	if !edi: return null # in case we tried to run scene, instead of use through editor
	
	var height = $ScrollContainer/VBoxContainer/Cylinder/Height.value
	var radius = $ScrollContainer/VBoxContainer/Cylinder/Radius.value
	
	AddCylinder(height, radius, radius, "Cylinder")

func _on_MakeCone_pressed():
	print ("Add capsule primitive")
	if !edi: return null # in case we tried to run scene, instead of use through editor
	
	var height = $ScrollContainer/VBoxContainer/Cone/Height.value
	var r1 = $ScrollContainer/VBoxContainer/Cone/RadiusBottom.value
	var r2 = $ScrollContainer/VBoxContainer/Cone/RadiusTop.value
	
	AddCylinder(height, r1, r2, "Cone")

func AddCylinder(height, r1, r2, base_name):
	var dest = GetDestParents()
	var options = GetOptions()
	
	var m
	if options["with_mesh"]:
		m = CylinderMesh.new()
		m.height = height
		m.bottom_radius = r1
		m.top_radius = r2
	var shape
	if options["with_collider"]:
		if r1 == r2:
			shape = CylinderShape.new()
			shape.radius = r1
			shape.height = height
		else:
			shape = m.create_convex_shape()
	
	for p in dest["parents"]:
		var nodes = BuildBase(options, p, dest["owner"])
		if nodes[0] != p: nodes[0].name = base_name
		if nodes[1]:
			nodes[1].mesh = m
			nodes[1].name = base_name + "Mesh"
			AdjustMaterial(nodes[1], options)
		if nodes[2]: #collider
			nodes[2].shape = shape
			nodes[2].name = base_name + "Shape"
		
		match options["axis"]:
			2:
				if nodes[1]: nodes[1].rotation_degrees = Vector3(0,90,90)
				if nodes[2]: nodes[2].rotation_degrees = Vector3(0,90,90)
			0:
				if nodes[1]: nodes[1].rotation_degrees = Vector3(0,0,90)
				if nodes[2]: nodes[2].rotation_degrees = Vector3(0,0,90)
#		match options["axis"]:
#			0: nodes[0].rotation_degrees = Vector3(0,0,90)	
#			1: nodes[0].rotation_degrees = Vector3(90,0,0)
#			2: nodes[0].rotation_degrees = Vector3(0,90,90)


func _on_MakePlane_pressed():
	var dest = GetDestParents()
	var options = GetOptions()
	
	var sizex = $ScrollContainer/VBoxContainer/Plane/Size.value
	var sizey = $ScrollContainer/VBoxContainer/Plane/Y.value
	if !$ScrollContainer/VBoxContainer/Plane/XY.pressed:
		sizey = sizex
		
	var m
	if options["with_mesh"]: 
		m = PlaneMesh.new()
		m.size = Vector2(sizex,sizey)
	var shape
	if options["with_collider"]:
		shape = BoxShape.new()
		shape.extents = Vector3(sizex/2, .005, sizey/2)
	
	for p in dest["parents"]:
		var nodes = BuildBase(options, p, dest["owner"])
		if nodes[0] != p: nodes[0].name = "Plane"
		if nodes[1]:
			nodes[1].mesh = m
			nodes[1].name = "PlaneMesh"
			AdjustMaterial(nodes[1], options)
		if nodes[2]: #collider
			nodes[2].shape = shape
			nodes[2].name = "PlaneShape"
			
		if options["axis"] == 2:
			if nodes[1]: nodes[1].rotation_degrees = Vector3(0,90,90)
			if nodes[2]: nodes[2].rotation_degrees = Vector3(0,90,90)
		elif options["axis"] == 0:
			if nodes[1]: nodes[1].rotation_degrees = Vector3(0,0,-90)
			if nodes[2]: nodes[2].rotation_degrees = Vector3(0,0,-90)


func _on_XYZ_toggled(button_pressed):
	if button_pressed:
		$ScrollContainer/VBoxContainer/Boxes/Size.prefix = "X:"
		$ScrollContainer/VBoxContainer/Boxes/Y.visible = true
		$ScrollContainer/VBoxContainer/Boxes/Z.visible = true
	else:
		$ScrollContainer/VBoxContainer/Boxes/Size.prefix = "Size:"
		$ScrollContainer/VBoxContainer/Boxes/Y.visible = false
		$ScrollContainer/VBoxContainer/Boxes/Z.visible = false


func _on_XY_toggled(button_pressed):
	if button_pressed:
		$ScrollContainer/VBoxContainer/Plane/Size.prefix = "X:"
		$ScrollContainer/VBoxContainer/Plane/Y.visible = true
	else:
		$ScrollContainer/VBoxContainer/Plane/Size.prefix = "Size:"
		$ScrollContainer/VBoxContainer/Plane/Y.visible = false
